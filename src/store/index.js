import { applyMiddleware, compose, createStore } from 'redux';
import rootReducer from '../reducers';
import cookie from 'js-cookie';
import rootSaga from '../sagas/rootSaga'
import createSagaMiddleware from 'redux-saga';
const SagaMiddleware=createSagaMiddleware ();
const composerEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const cartItems = cookie.getJSON('cartItem') || [];
// const userInfos= cookie.getJSON('userInfos') || null;
// ,userSignIn:{loading:false, userInfos} 
const initialState = { cart:{cartItems} };

const store = createStore(
  rootReducer,
  initialState,
  composerEnhancer(applyMiddleware(SagaMiddleware))
);
SagaMiddleware.run(rootSaga);
export default store;