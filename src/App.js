import React from 'react';
import Home from './components/homeComponent/home';
import Login from './components/Login-RegisterComponent/login';
import NavBarbottom from './components/navBottomComponent/navBar-bottom';
import Register from './components/Login-RegisterComponent/register';
import Favorite from './components/favoriteComponent/favorite';
import ProductContainer from './container/productContainer';
import User from './components/userComponent/user';
import Notification from './components/notificationComponet/notification';
import ItemContainer from './container/ItemContainer';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faTrash,
  faPizzaSlice,
  faCookieBite,
  faHamburger,
  faBirthdayCake,
  faSearch,
  faHeart,
  faShoppingBasket,
  faUser,
  faHome,
  faArrowLeft,
  faCheck,
  faPlus,
  faStar,
  faBell,
} from '@fortawesome/free-solid-svg-icons';
import ProductScreen from './components/productDetailComponent/productScreen';
import AdminPanel from './components/adminPanelComponent/adminPanel';
library.add(
  faTrash,
  faPizzaSlice,
  faCookieBite,
  faHamburger,
  faBirthdayCake,
  faSearch,
  faBell,
  faHeart,
  faShoppingBasket,
  faUser,
  faHome,
  faArrowLeft,
  faCheck,
  faPlus,
  faStar
);
const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact render={(props) => <Login {...props} />} />
        <Route path="/product" render={(props) => <Home {...props} />} />
        <Route path="/favorite">
          <Favorite />
          <NavBarbottom />
        </Route>
        <Route path="/notification">
          <Notification />
          <NavBarbottom />
        </Route>
        <Route
          path="/ProductScreen/:item?/:id"
          render={(props) => <ProductScreen {...props} />}
        />

        <Route
          path="/cart/:item?/:id?"
          render={(props) => <ItemContainer {...props} />}
        />
        <Route path="/register" render={(props) => <Register {...props} />} />
        <Route
          path="/ProductContainer"
          render={(props) => <ProductContainer {...props} />}
        />
        <Route path="/adminPanel">
          <AdminPanel />
     
        </Route>
        <Route path="/user">
          <User />
          <NavBarbottom />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
