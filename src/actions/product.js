import * as types from '../constants/';

export const handleAdd = (item, id) => {
  switch (item) {
    case 'burger':
      return { type: types.BURGER_ADDED_SAGA, payload: id };
    case 'cookies':
      return { type: types.COOKIE_ADDED_SAGA, payload: id };
    case 'cakes':
      return { type: types.CAKE_ADDED_SAGA, payload: id };
    case 'pizza':
      return { type: types.PIZZA_ADDED_SAGA, payload: id };
    default:
      break;
  }
};

export const getBurgers = () =>  {
  return { type: types.BURGER_LIST_REQUEST_SAGA };
};
export const getCookies = () => {
  return{ type: types.COOKIES_LIST_REQUEST_SAGA }
};

export const getPizzas = () => {
 return { type: types.PIZZA_LIST_REQUEST_SAGA }
};
export const getCakes = () =>  {
  return { type: types.CAKES_LIST_REQUEST_SAGA }
};

export const detailProduct = (item, id) => {
 switch (item) {
  case 'burger':
    return {type:types.BURGER_DETAIL_REQUEST_SAGA,payload:id}
    case 'cookies':
      console.log('itew',item,id)
    return {type:types.COOKIE_DETAIL_REQUEST_SAGA,payload:id} 
    case 'cakes':
      console.log('itew',item,id)
      return {type:types.CAKE_DETAIL_REQUEST_SAGA,payload:id}
      case 'pizza':
        return {type:types.PIZZA_DETAIL_REQUEST_SAGA,payload:id}
  default:
    break;
}
};
// export const findProduct = (value) => {
//   return {
//     type: types.SEARCH_PRODUCT,
//     value,
//   };
// };
export const addToCart = (item, id, qty) =>  {

  switch (item) {
    case 'burger':
      return {type:types.ADD_BURGER_TO_CART_SAGA,payload:{id,qty}}
      case 'cookies':
      return {type:types.ADD_COOKIE_TO_CART_SAGA,payload:{id,qty}} 
      case 'cakes':
        return {type:types.ADD_CAKE_TO_CART_SAGA,payload:{id,qty}}
        case 'pizza':
          return {type:types.ADD_PIZZA_TO_CART_SAGA,payload:{id,qty}}
    default:
      break;
  }

};
export const removeFromCart = (id) => {
  return{
    type: types.REMOVE_FROM_CART_SAGA,
    payload: id,
  };
  // const {
  //   cart: { cartItem },
  // } = getState();
  // // cookie.set('cartItems', JSON.stringify(cartItem));
};
export const incrementProduct = (id) => {
  return {
    type: types.INCREMENT_QUANTITY_SAGA,
    payload: id,
  };
};
export const decrementProduct = (id) => {
  return {
    type: types.DECREMENT_QUANTITY_SAGA,
    payload: id,
  };
};
