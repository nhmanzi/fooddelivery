import {all} from 'redux-saga/effects'
import {addBurgertWatcher,addCakeWatcher,addCookieWatcher,
      addPizzaWatcher,getBurgersWatcher,getCookieWatcher,
      getPizzaWatcher,getcakeWatcher,burgerDetailWatcher,
      cakeDetailWatcher,cookieDetailWatcher,pizzaDetailWatcher,
      handleBurgerAddWatcher,handleCakeAddWatcher,handleCookieAddWatcher,
      handlePizzaAddWatcher,removeFromCartWatcher,decrementProductWatcher,incrementProductWatcher} from './productSaga';
import {userSignInWatcher,userSignOutWatcher,userRegisterInWatcher }from './userSaga';


export default function* rootSaga (){
    yield all([ 
        addBurgertWatcher(),addCakeWatcher(),addCookieWatcher(),addPizzaWatcher(),
        getBurgersWatcher(),getCookieWatcher(),getPizzaWatcher(),getcakeWatcher(),
        burgerDetailWatcher(),cakeDetailWatcher(),cookieDetailWatcher(),pizzaDetailWatcher(),
        handleBurgerAddWatcher(),handleCakeAddWatcher(),handleCookieAddWatcher(),
        handlePizzaAddWatcher(),removeFromCartWatcher(),decrementProductWatcher(),
        incrementProductWatcher(),userSignInWatcher(),userSignOutWatcher(),userRegisterInWatcher()
    ])
}