import {put,takeEvery,call } from  'redux-saga/effects'
import * as types from '../constants'
import { fetchBurgers, fetchCakes, fetchCookies, fetchPizza, fetchSingleBurger,fetchSinglePizza,fetchSingleCake,fetchSingleCookie } from '../services/product-services';


export function* getBurgerSaga ()  {
    try {
    yield  put({ type: types.BURGER_LIST_REQUEST });
      const  data  = yield call(fetchBurgers)
     yield put({ type: types.BURGER_LIST_SUCCESS, payload: data });
    } catch (error) {
    yield  put({ type: types.BURGER_LIST_FAIL, payload: error.message });
    }
  };

  export function* getPizzaSaga ()  {
    try {
    yield  put({ type: types.PIZZA_LIST_REQUEST });
      const  data  = yield call(fetchPizza)
     yield put({ type: types.PIZZA_LIST_SUCCESS, payload: data });
    } catch (error) {
    yield  put({ type: types.PIZZA_LIST_FAIL, payload: error.message });
    }
  };



  export function* getCakeSaga ()  {
    try {
    yield  put({ type: types.CAKES_LIST_REQUEST });
    const  data  = yield call(fetchCakes)
     yield put({ type: types.CAKES_LIST_SUCCESS, payload: data });
    } catch (error) {
    yield  put({ type: types.CAKES_LIST_FAIL, payload: error.message });
    }
  };


  export function* getCookieSaga ()  {
    try {
    yield  put({ type: types.COOKIES_LIST_REQUEST });
      const data = yield call(fetchCookies)
     yield put({ type: types.COOKIES_LIST_SUCCESS, payload: data });
    } catch (error) {
    yield  put({ type: types.COOKIES_LIST_FAIL, payload: error.message });
    }
  };


  export function*  burgerDetailSaga  (action)  {
        try {
        yield put({type:types.PRODUCT_DETAIL_REQUEST})
          const  data  = yield call(fetchSingleBurger,action.payload)
         yield put({ type: types.PRODUCT_DETAIL_SUCCESS, payload: data });
        } catch (error) {
        yield  put({ type: types.PRODUCT_DETAIL_FAIL, payload: error.message });
        }
    }
    export function*  pizzaDetailSaga  (action)  {
        
        try {
        const  data = yield  call(fetchSinglePizza,action.payload)
        yield   put({ type: types.PRODUCT_DETAIL_SUCCESS, payload: data });
        } catch (error) {
       yield   put({ type: types.PRODUCT_DETAIL_FAIL, payload: error.message });
        }
    }
    export function*  cookieDetailSaga  (action)  {
        
        try {
         
          const  data  = yield call(fetchSingleCookie,action.payload)
        yield  put({ type: types.PRODUCT_DETAIL_SUCCESS, payload: data });
        } catch (error) {
        yield  put({type: types.PRODUCT_DETAIL_FAIL, payload: error.message });
        }
      }
      export function*  cakeDetailSaga  (action)  {
       
        try {
         
        const data = yield call(fetchSingleCake,action.payload)
        yield  put({ type: types.PRODUCT_DETAIL_SUCCESS, payload: data });
        } catch (error) {
        yield  put({ type: types.PRODUCT_DETAIL_FAIL, payload: error.message });
        }
    }
  
export function* addBurgerSaga (action){
    const{id,qty}=action.payload
    const  data  = yield call(fetchSingleBurger,id)
          yield  put({
              type: types.ADD_TO_CART,
              payload: {
                product: data.id,
                title: data.title,
                company: data.companyName,
                price: data.price,
                countStock: data.stockCount,
                rating: data.rating,
                item: data.item,
                isAdded: true,
                photo: data.photo,
                qty,
              },
            });
    }
    
         
          
    export function* addPizzaSaga (action){
        const{id,qty}=action.payload
        const  data  = yield call(fetchSinglePizza,id)
           yield put({
              type: types.ADD_TO_CART,
              payload: {
                product: data.id,
                title: data.title,
                company: data.companyName,
                price: data.price,
                countStock: data.stockCount,
                rating: data.rating,
                item: data.item,
                photo: data.photo,
                qty,
              },
            });
        }
    export function* addCookieSaga (action){
            const{id,qty}=action.payload
            const  data  = yield call(fetchSingleCookie,id)
           yield put({
              type: types.ADD_TO_CART,
              payload: {
                product: data.id,
                title: data.title,
                company: data.companyName,
                price: data.price,
                countStock: data.stockCount,
                rating: data.rating,
                item: data.item,
                photo: data.photo,
                qty,
              },
            });
        }
         
    
     export function* addCakeSaga (action){
            const{id,qty}=action.payload
            const data  = yield call(fetchSingleCake,id)
           yield   put({
              type: types.ADD_TO_CART,
              payload: {
                product: data.id,
                title: data.title,
                company: data.companyName,
                price: data.price,
                countStock: data.stockCount,
                rating: data.rating,
                item: data.item,
                photo: data.photo,
                qty,
              },
            });
            // const {
            //   cart: { cartItem },
            // } = yield getState();
            // cookie.set('cartItems', JSON.stringify(cartItem));
        }   
  
//handle adding--------------------------->

export function* handleBurgerAddSaga(action){
   yield put({type:types.BURGER_ADDED,payload:action.payload})
}


export function* handlePizzaAddSaga(action){
  yield put({type:types.PIZZA_ADDED,payload:action.payload})
}


export function* handleCookieAddSaga(action){
  yield put({type:types.COOKIE_ADDED,payload:action.payload})
}


export function* handleCakeAddSaga(action){
  yield put({type:types.CAKE_ADDED,payload:action.payload})
}



export function*  removeFromCartSaga (action){
  yield put({type:types.REMOVE_FROM_CART,payload:action.payload})
}


export function* incrementProductSaga(action){
  yield put({type:types.INCREMENT_QUANTITY,payload:action.payload})
}

export function* decrementProductSaga(action){

  yield put({type:types.DECREMENT_QUANTITY,payload:action.payload})
}

// generator watchers----------------------------------------------------->

export function* getBurgersWatcher(){
  yield takeEvery(types.BURGER_LIST_REQUEST_SAGA,getBurgerSaga)
}

export function* getPizzaWatcher(){
  yield takeEvery(types.PIZZA_LIST_REQUEST_SAGA,getPizzaSaga)
}

export function* getcakeWatcher(){
  yield takeEvery(types.CAKES_LIST_REQUEST_SAGA,getCakeSaga)
}

export function* getCookieWatcher(){
  yield takeEvery(types.COOKIES_LIST_REQUEST_SAGA,getCookieSaga)
}


export function* burgerDetailWatcher(){
  yield takeEvery(types.BURGER_DETAIL_REQUEST_SAGA,burgerDetailSaga)
}
export function* cookieDetailWatcher(){
  yield takeEvery(types.COOKIE_DETAIL_REQUEST_SAGA,cookieDetailSaga)
}
export function* cakeDetailWatcher(){
  yield takeEvery(types.CAKE_DETAIL_REQUEST_SAGA,cakeDetailSaga)
}
export function* pizzaDetailWatcher(){
  yield takeEvery(types.PIZZA_DETAIL_REQUEST_SAGA,pizzaDetailSaga)
}

export function* addBurgertWatcher(){
  yield takeEvery(types.ADD_BURGER_TO_CART_SAGA,addBurgerSaga)
}
export function* addCakeWatcher(){
  yield takeEvery(types.ADD_CAKE_TO_CART_SAGA,addCakeSaga)
}
export function* addCookieWatcher(){
  yield takeEvery(types.ADD_COOKIE_TO_CART_SAGA,addCookieSaga)
}
export function* addPizzaWatcher(){
  yield takeEvery(types.ADD_PIZZA_TO_CART_SAGA,addPizzaSaga)
}

export function* handleBurgerAddWatcher(){
  yield takeEvery(types.BURGER_ADDED_SAGA,handleBurgerAddSaga)
}

export function* handlePizzaAddWatcher(){
  yield takeEvery(types.PIZZA_ADDED_SAGA,handlePizzaAddSaga)
 }

 export function* handleCookieAddWatcher(){
  yield takeEvery(types.COOKIE_ADDED_SAGA,handleCookieAddSaga)
 }

 export function* handleCakeAddWatcher(){
  yield takeEvery(types.CAKE_ADDED_SAGA,handleCakeAddSaga)
 }

 export function* removeFromCartWatcher(){
  yield takeEvery(types.REMOVE_FROM_CART_SAGA,removeFromCartSaga)
}

export function* incrementProductWatcher(){
  yield takeEvery(types.INCREMENT_QUANTITY_SAGA,incrementProductSaga)
}

export function* decrementProductWatcher(){
  yield takeEvery(types.DECREMENT_QUANTITY_SAGA,decrementProductSaga)
}
