import * as types  from "../constants"
import axios from 'axios'
import {put,takeEvery} from 'redux-saga/effects'
export function* userSignInSaga (action){
        yield put({type:types.USER_SIGNIN_REQUEST})
        try {
            const { data } = yield axios.post(
              'http://localhost:5000/api/auth/login',
              action.payload
            )         
   yield put({type:types.USER_SIGNIN_SUCCESS,payload:data})
    
    
}catch(error){

   yield put({type:types.USER_SIGNIN_FAIL,payload:error.response.data.message})
}
}

export function* userSignInWatcher (){
    yield takeEvery(types.USER_SIGNIN_REQUEST_SAGA,userSignInSaga)
}

export function* userRegisterInSaga (action){
    yield put({type:types.USER_REGISTER_REQUEST})
    try {
        const { data } = yield axios.post(
          'http://localhost:5000/api/auth/signup',
          action.payload
        )
      console.log(data)

yield put({type:types.USER_REGISTER_SUCCESS,payload:{message:'Registered!'}})


}catch(error){

yield put({type:types.USER_REGISTER_FAIL,payload:error.response.data.message})
}
}

export function* userRegisterInWatcher (){
    yield takeEvery(types.USER_REGISTER_REQUEST_SAGA,userRegisterInSaga )
}

export function* userSignOutSaga (){
        yield put({type:types.USER_SIGNOUT})
    }
  
export function* userSignOutWatcher (){
    yield takeEvery(types.USER_SIGNOUT_SAGA,userSignOutSaga)
}
