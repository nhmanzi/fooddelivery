import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useHistory } from 'react-router-dom';
import Headers from '../headerComponent/headers';
import { useSelector } from 'react-redux';
const User = (props) => {
  const history = useHistory();
  const userSignIn = useSelector(state => state.userSignIn)
  const {userInfos}= userSignIn
  console.log('INFOOOOOOOOOOO',userInfos)
  return (
    <div>
      <Headers />
      <div className="container">
        <div onClick={() => history.goBack()}>
          <button
            className="btn btn-light"
            style={{ border: '.5px solid #e5e2e2', marginTop: '5px' }}
          >
            <FontAwesomeIcon className="faicons" icon="arrow-left" />
          </button>
        </div>
        <h3>User</h3>
        <hr />
        {userInfos?
        
<ul className="list-group mb-4 checkout-invisible text-center">
          <li className="list-group-item list-group-item-warning d-flex justify-content-between lh-condensed">
          <div style={{display:'flex', alignItems:'center'}}>
          <h6 style={{margin:0 , paddingRight:'1vw'}}>Names:</h6>  
         <span style={{margin:0}}>{userInfos.name}</span> 
        </div>
</li>
<li className="list-group-item  d-flex justify-content-between lh-condensed">
        <div style={{display:'flex'}}>
            <h6 style={{margin:0, paddingRight:'1vw'}}>Email:</h6>
         <span style={{margin:0}}>{userInfos.email}</span> 
        </div>
        </li>
        </ul>
        :<ul className="list-group mb-4 checkout-invisible text-center">
          <li className="list-group-item list-group-item-danger d-flex justify-content-between lh-condensed">
            <div className="col">
              <h6>no User</h6>
            </div>
          </li>
        </ul>}
      </div>
    </div>
  );
};

export default User;
