import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { detailProduct } from '../../actions/product';
import '../../styling/productScreen.css';
import NavBarbottom from '../navBottomComponent/navBar-bottom';
import Header from '../headerComponent/headers';
import Stars from '../shared/stars';
function ProductScreen(props) {
  const [qty, setQty] = useState(1);
  const productDetail = useSelector((state) => state.productDetail);
  const { product, loading } = productDetail;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(detailProduct(props.match.params.item, props.match.params.id));
    return () => {
      // cleanup;
    };
  }, [props.match.params.item, props.match.params.id,dispatch]);
  const history = useHistory();
  const handleCart = () => {
    history.push(`/cart/${product.item}/${product.id}?qty=${qty}`);
  };
  return loading ? (
    <div>loading....</div>
  ) : (
    <>
      <Header />
      <div className="container productMain">
        <div onClick={() => history.goBack()}>
          <button
            className="btn btn-light"
            style={{ border: '.5px solid #e5e2e2', marginTop: '5px' }}
          >
            <FontAwesomeIcon className="faicons" icon="arrow-left" />
          </button>
        </div>
        <div className="Product">
          <div className="image">
            <img src={product.photo} alt={product.title} />
          </div>
          <div className="details">
            <div className="tittle">
              <strong>{product.title}</strong>
            </div>
            <div className="rating">
              <Stars rating={product.rating} />
            </div>
            <div className="price">
              <span>
                <strong>price</strong>
              </span>
              <strong
                style={{ color: 'red' }}
                className="ml-2"
              >{`${product.price} rwf`}</strong>
            </div>
            <div className="quantity">
              <div>
                <strong>Quantity</strong>
              </div>
              <div className="ml-2">
                <select onChange={(e) => setQty(e.target.value)}>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                </select>
              </div>
            </div>
            <button className="btn btn-danger p-1" onClick={handleCart}>
              add cart
            </button>
          </div>
        </div>
        <NavBarbottom />
      </div>
    </>
  );
}

export default ProductScreen;
