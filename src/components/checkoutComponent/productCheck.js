import React from 'react';
import { getCartTotal } from '../../reducers/product';
// import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Redirect, useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
const Checkout = (props) => {
   const userSignIn = useSelector(state => state.userSignIn)
  const {loading,userInfos,error}= userSignIn
  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;
  const history = useHistory();
  return ( userInfos?
    (<div className="container">
      <div onClick={() => history.goBack()}>
        <button
          className="btn btn-light"
          style={{ border: '.5px solid #e5e2e2', marginTop: '10px' }}
        >
          <FontAwesomeIcon className="faicons" icon="arrow-left" />
        </button>
      </div>
      <div className="row">
        <div className="col-md-4 order-md-2 mb-4">
          <h4 className="d-flex justify-content-between align-items-center mt-3 mb-3">
            <span className="text-muted">Your cart</span>
            <span className="badge badge-danger badge-pill">
              {cartItems.length}
            </span>
          </h4>
          <ul className="list-group mb-3">
          <li className="list-group-item d-flex justify-content-between">
              <strong>Product</strong>
              <strong>Price</strong>
            </li>
            {cartItems.map((e) => (
              <li
                className="list-group-item d-flex justify-content-between lh-condensed"
                key={e.title}
              >
                <div>
                  <h6 className="my-0">{e.title}</h6>
                  <small className="text-muted">{e.companyName}</small>
                </div>
                <span className="text-muted">{e.price}</span>
              </li>
            ))}
            <li className="list-group-item d-flex justify-content-between">
              <span>Total (Rwf)</span>
              <strong>{`rwf ${getCartTotal(cartItems)}`} </strong>
            </li>
          </ul>
        </div>
        <div className="col-md-8 order-md-1">
          <h4 className="mb-3 mt-3">Billing address</h4>
          <form className="needs-validation mb-3">
            <div className="row">
              <div className="col-md-6 mb-3">
                <label id="firstName">First name</label>
                <input
                  type="text"
                  className="form-control"
                  id="firstName"
                  placeholder=""
                  required
                />
                <div className="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div className="col-md-6 mb-3">
                <label id="lastName">Last name</label>
                <input
                  type="text"
                  className="form-control"
                  id="lastName"
                  placeholder=""
                  required
                />
                <div className="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>
            <div className="mb-3">
              <label id="username">Username</label>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">@</span>
                </div>
                <input
                  type="text"
                  className="form-control col-6"
                  id="username"
                  placeholder="Username"
                  required
                />
              </div>
            </div>

            <div className="mb-3">
              <label id="email">
                Email <span className="text-muted">(Optional)</span>
              </label>
              <input
                type="email"
                className="form-control"
                id="email"
                placeholder="you@example.com"
              />
              <div className="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>

            <div className="mb-3">
              <label id="address">Address</label>
              <input
                type="text"
                className="form-control"
                id="address"
                placeholder="1234 Main St"
                required
              />
              <div className="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <hr className="mb-4" />
            <div className="custom-control custom-checkbox">
              <input
                type="checkbox"
                className="custom-control-input"
                id="same-address"
              />
              <label className="custom-control-label" id="same-address">
                Shipping address is the same as my billing address
              </label>
            </div>

            <hr className="mb-4" />

            <h4 className="mb-3">Payment</h4>

            <div className="d-block my-3">
              <div className="custom-control custom-radio">
                <input
                  id="credit"
                  name="paymentMethod"
                  type="radio"
                  className="custom-control-input"
                  required
                />
                <label className="custom-control-label" id="credit">
                  Credit card
                </label>
              </div>
              <div className="custom-control custom-radio">
                <input
                  id="debit"
                  name="paymentMethod"
                  type="radio"
                  className="custom-control-input"
                  required
                />
                <label className="custom-control-label" id="debit">
                  Debit card
                </label>
              </div>
              <div className="custom-control custom-radio">
                <input
                  id="paypal"
                  name="paymentMethod"
                  type="radio"
                  className="custom-control-input"
                  required
                />
                <label className="custom-control-label" id="paypal">
                  PayPal
                </label>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 mb-3">
                <label id="cc-name">Name on card</label>
                <input
                  type="text"
                  className="form-control"
                  id="cc-name"
                  placeholder=""
                  required
                />
                <small className="text-muted">
                  Full name as displayed on card
                </small>
                <div className="invalid-feedback">Name on card is required</div>
              </div>
              <div className="col-md-6 mb-3">
                <label id="cc-number">Credit card number</label>
                <input
                  type="text"
                  className="form-control"
                  id="cc-number"
                  placeholder=""
                  required
                />
                <div className="invalid-feedback">
                  Credit card number is required
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-3 mb-3">
                <label id="cc-expiration">Expiration</label>
                <input
                  type="text"
                  className="form-control"
                  id="cc-expiration"
                  placeholder=""
                  required
                />
                <div className="invalid-feedback">Expiration date required</div>
              </div>
              <div className="col-md-3 mb-3">
                <label id="cc-cvv">CVV</label>
                <input
                  type="text"
                  className="form-control"
                  id="cc-cvv"
                  placeholder=""
                  required
                />
                <div className="invalid-feedback">Security code required</div>
              </div>
            </div>
            <hr className="mb-4" />
            <button className="btn btn-danger btn-lg btn-block" type="submit">
              Confirm
            </button>
          </form>
        </div>
      </div>
    </div>):<Redirect to='/'/>
  );
};

export default Checkout;
