import React from 'react';
import { useHistory } from 'react-router-dom';
import Slider from 'react-slick';
// import PropTypes from 'prop-types';
import '../../styling/singleProduct.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Svgs from '../shared/svg';
import Stars from '../shared/stars';
import { useDispatch } from 'react-redux';
import { addToCart, handleAdd } from '../../actions/product';
const ItemPizza = ({ pizza }) => {
  const dispatch = useDispatch();
  let settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 990,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  const handleAdd1 = (item, id) => {
    dispatch(handleAdd(item, id));
    dispatch(addToCart(item, id, 1));
  };
  const history = useHistory();
  return (
    <div style={{display:'flex', flexDirection:"column", alignItems:"center", justifyContent:'center'}}>
    <Slider {...settings}>
      {pizza.map((pizza) => (
        <div className="display-body " key={pizza.id}>
          <Svgs />
          <div className="img-container">
            <img
              onClick={() =>
                history.push(`/productScreen/${pizza.item}/${pizza.id}`)
              }
              className=".img-fluid "
              src={pizza.photo}
              alt={pizza.title}
            />
          </div>
          <div>
            <h5>{pizza.title}</h5>
          </div>
          <div>
            <p>{pizza.companyName}</p>
          </div>

          <div className="red">
            <h3>{`Rwf ${pizza.price}`}</h3>
          </div>
          <div>
            <Stars rating={pizza.rating} id={pizza.title} />
          </div>

          {pizza.isAdded === true ? (
            <div className="addFlag p-2">
              <div>
                <h6> added</h6>{' '}
              </div>
              <div
                style={{
                  height: '20px',
                  width: '20px',
                  padding: '5px',
                }}
                className="badge badge-success rounded-circle checkFlag"
              >
                <FontAwesomeIcon
                  style={{
                    color: 'white',
                  }}
                  className="faicons"
                  icon="check"
                />
              </div>
            </div>
          ) : null}

          {pizza.isAdded === false ? (
            <button
              style={{
                height: '40px',
                width: '40px',
              }}
              className="btn add rounded-circle btn-danger"
              onClick={() => handleAdd1(pizza.item, pizza.id)}
            >
              <FontAwesomeIcon
                style={{
                  color: 'white',
                }}
                className="faicons"
                icon="plus"
              />
            </button>
          ) : null}
          <br />
        </div>
      ))}
    </Slider>
    </div>
  );
};
// ItemPizza.propTypes = {
//   pizza: PropTypes.arrayOf(PropTypes.object),
//   onAdd: PropTypes.func,
// };
export default ItemPizza;
