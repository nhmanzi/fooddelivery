import React from 'react';
// import PropTypes from 'prop-types';
import ItemPizza from './itemPizza';

const Pizza = ({ pizza }) => {
  return <ItemPizza pizza={pizza} />;
};
// Pizza.propTypes = {
//   pizza: PropTypes.arrayOf(PropTypes.object),
//   onAdd: PropTypes.func,
// };
export default Pizza;
