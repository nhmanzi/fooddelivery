import React from 'react';
// import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import '../../styling/NavBarbottom.css';
export const NavBarbottom = () => {
  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;
  const userSignIn = useSelector(state => state.userSignIn)
  const {userInfos}= userSignIn
  console.log('INFOOOOOOOOOOO mannn',userInfos)
  return (
    <nav style={{ zIndex: 100,padding:0,position:'fixed', bottom:0,left:0,right:0,margin:'auto', width:'95vw',display:'flex',	height: '10vh',backgroundColor:'white',borderRadius:'10px',	border: '1px solid #e3e5e9',alignItems:'center',justifyContent:'center'}}>
      <span className="taskbar  ">
        <NavLink to="/product/burger">
          <FontAwesomeIcon
            style={{
              color: '#111',
              backgroundColor: 'transparent',
            }}
            className="faicons"
            icon="home"
          />
        </NavLink>
        <NavLink to="/favorite">
          {' '}
          <FontAwesomeIcon
            style={{
              color: '#a8acb1',
              backgroundColor: 'transparent',
            }}
            className="faicons"
            icon="heart"
          />
        </NavLink>
        <NavLink to="/cart">
          <div
            style={{
              position: 'relative',
            }}
          >
            {/* <p className="cart-number">{productCount.length}</p> */}
            <p className="cart-number">{cartItems.length}</p>
            <FontAwesomeIcon
              style={{
                color: '#a8acb1',
                backgroundColor: 'white',
                padding: '4px',
                borderRadius: '50%',
                boxShadow: ' 3px 3px 4px #c1c5cb, -3px -3px 4px #fff',
              }}
              className="faicons basket"
              icon="shopping-basket"
            />
          </div>
        </NavLink>
        <NavLink to="/notification">
          {' '}
          <FontAwesomeIcon
            style={{
              color: '#a8acb1',
              backgroundColor: 'transparent',
            }}
            className="faicons"
            icon="bell"
          />
        </NavLink>
          {userInfos?<NavLink to="/user"><div className="userinfo"><p style={{margin:0,padding:0}}>US</p></div></NavLink>:(<NavLink to="/user">
          <FontAwesomeIcon
            style={{
              color: '#a8acb1',
              backgroundColor: 'transparent',
            }}
            className="faicons"
            icon="user"
          />
        </NavLink>)}
      </span>
    </nav>
  );
};
// NavBarbottom.propTypes = {
//   productCount: PropTypes.arrayOf(PropTypes.object),
//   match: PropTypes.string,
// };

// const mapStateToProps = (store) => {
//   return {
//     productCount: store.product.temp,
//   };
// };

export default NavBarbottom;
