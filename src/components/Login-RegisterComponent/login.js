import React, { useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import * as Yup from 'yup';
 import { Formik, Form,Field } from 'formik';
import '../../styling/login.css';
import {signIn} from  '../../actions/user';
import { Link } from 'react-router-dom';
import { TextFormField } from '../shared/TextFormField';
const Login =(props)=> {
  const signinSchema= Yup.object().shape({
    password:Yup.string()
    .required()
      .min(4,'password too short'),
      email:Yup.string()
      .required()
      .min(4,'email too short')
      .email()
  })
  const dispatch = useDispatch()
  const userSignIn = useSelector(state => state.userSignIn)
  const {loading,userInfos,error}= userSignIn
 useEffect(() => {
   if(userInfos){
    if(userInfos.isAdmin===false) props.history.push('/product/burger')
    else props.history.push('/adminPanel')
   }
 }, [userInfos,props.history])
const login=(values)=>{
  dispatch(signIn(values));
}
    return (
      <div >
        <Formik
       initialValues={{ email: '', password: '' }}
       validationSchema={signinSchema}
       onSubmit={(values)=>login(values)}
     >
     {() => (
        <Form className="form-signin" style={{ marginTop: '20vh' }}>
          
        <div className='text-center'>
          <h3>SignIn</h3>
        </div>
        <br/>
       {loading && <div style={{color:'lightgreen'}}>loading ..</div>}
     {error && <div style={{color:'red',margin:'0px auto'}}>{error}</div>}
     <br/>
        <Field label='Email' type="email" name="email" component={TextFormField} />
        <Field label='Password' name='password' type="password" component={TextFormField} />

           <br />   <br /> 
            <button type="submit"  className="btn btn-lg btn-danger btn-block"  >
            SignIn</button>
              <br />
        <button className="btn btn-secondary btn-block" onClick={()=>props.history.push('/product/burger')}>signIn later</button>
          <br />
          <div>
            Don't have an account
            <Link to="/register" style={{ color: 'red' }}>
              {' '}
              <u>Create Account</u>
            </Link>
          </div>
          </Form>
       )}
     </Formik>
   </div>
 );
       }

export default Login;

