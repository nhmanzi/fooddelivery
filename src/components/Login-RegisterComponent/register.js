import React from 'react';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {useDispatch, useSelector} from 'react-redux';
import '../../styling/register.css';
import * as Yup from 'yup';
import { TextFormField } from '../shared/TextFormField';
 import { Formik, Form,Field } from 'formik';
import { register } from '../../actions/user';
const  Register=(props)=>{
  const registerSchema= Yup.object().shape({
    firstName:Yup.string().required('firstName is required').min(3,'name is too short'),
    lastName:Yup.string().required('LastName is required').min(3,'name is too short'),
    email:Yup.string().required('email is required').min(4,'email is too short').email(),
    password:Yup.string().required('password is required').min(4,'password is too short'),
    retype:Yup.string().required('password is required').min(4,'password is too short') 
  })
  const history=useHistory();
  const userRegister = useSelector(state => state.userRegister)
  const{error,loading,message}=userRegister
  const dispatch = useDispatch()
  const Register =(values)=>{
    dispatch(register(values));
  }
 
    return (
      <Formik
      initialValues={{ email: '', password: '' }}
      validationSchema={registerSchema}
      onSubmit={(values)=>Register(values)}
    >
    {() => (
       <Form className="form-signin" style={{ marginTop: '15vh' }}>
          <div onClick={() => history.push('/')}>
        <button
          className="btn btn-light"
          style={{ border: '.5px solid #e5e2e2', marginTop: '5px' }}
        >
          <FontAwesomeIcon className="faicons" icon="arrow-left" />
        </button>
      </div>
       <div className='text-center'>
         <h3>Register</h3>
       </div>
       <br/>
      {loading && <div className="loader2" style={{color:'lightgreen'}}>loading ..</div>}
    {error && <div style={{color:'red',margin:'0px auto'}}>{error}</div>}
    {message?<ul className="list-group mb-4 checkout-invisible text-center">
          <li className="list-group-item list-group-item-success d-flex justify-content-center lh-condensed">
          <div>
    <h6 style={{textAlign:'center'}}>{message.message}</h6>  
        </div>
</li></ul>:null}

       <Field label='FirstName' type="text" name="firstName" component={TextFormField} disabled={message} />
       <Field label='LastName' type="text" name="lastName" component={TextFormField} disabled={message}/>
       <Field label='Email' type="email" name="email" component={TextFormField} disabled={message}/>
       <Field label='Password' name='password' type="password" component={TextFormField} disabled={message} />
       <Field label='Retype' name='retype' type="password" component={TextFormField} disabled={message}/>
          <br />   <br /> 
         
          
{!message?<button class="btn btn-lg btn-danger btn-block mt-4" type="submit">
            Sign Up
          </button>:<button  className="btn btn-lg btn-primary btn-block" onClick={()=>props.history.push('/')}  >
              Sign in to continue</button>}
         </Form>
      )}
    </Formik>
    );

}

export default Register;

//onClick={(event) => this.signUp(event)}
