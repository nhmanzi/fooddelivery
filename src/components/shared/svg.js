import React, { useState } from 'react';

function Svgs() {
  const [style, setStyle] = useState('without-fill');
  let css = style === 'without-fill' ? 'with-fill' : 'without-fill';
  function changestyle() {
    setStyle(css);
  }

  return (
    <div style={{ position: 'absolute', zIndex: 100, right: 20, top: 10 }}>
      <svg
        id="Layer_1"
        data-name="Layer 1"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 80 70.01"
        height="30"
        width="30"
        onClick={() => changestyle()}
      >
        <title>heartOg</title>
        <path
          d="M66,14c-7,0-13,3.74-18,10.08C43,17.74,37,14,30,14A22,22,0,0,0,8,36c0,8.2,6.18,15.91,13.7,23.82S38.5,75.74,45.88,83.13a3,3,0,0,0,4.24,0c7.38-7.39,16.65-15.4,24.18-23.31S88,44.2,88,36A22,22,0,0,0,66,14Zm4,41.69c-6.58,6.92-14.77,14.15-22,21.15-7.18-7-15.37-14.23-21.95-21.15C18.82,48.1,14,40.81,14,36A16,16,0,0,1,30,20c5.86,0,10.73,3.12,15.46,10.61a3,3,0,0,0,4.14.93,3.09,3.09,0,0,0,.94-.93C55.27,23.12,60.14,20,66,20A16,16,0,0,1,82,36C82,40.81,77.18,48.1,70,55.69Z"
          transform="translate(-8 -14)"
          style={{ fill: '#ef251b' }}
        />
        <path
          d="M82,36c0,4.81-4.82,12.1-12,19.69-6.58,6.92-14.77,14.15-22,21.15-7.18-7-15.37-14.23-21.95-21.15C18.82,48.1,14,40.81,14,36A16,16,0,0,1,30,20c5.86,0,10.73,3.12,15.46,10.61a3,3,0,0,0,4.14.93,3.09,3.09,0,0,0,.94-.93C55.27,23.12,60.14,20,66,20A16,16,0,0,1,82,36Z"
          transform="translate(-8 -14)"
          className={style}
        />
      </svg>
    </div>
  );
}

export default Svgs;
