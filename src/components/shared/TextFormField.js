import {getIn} from 'formik';
import React from 'react';
import {TextField} from '@material-ui/core';
export const TextFormField=({field,form,...props})=> {
const erroxText=
getIn(form.touched, field.name)&& getIn(form.errors,field.name);

    return (
     <TextField
     fullWidth
     margin='none'
     helperText={erroxText}
     error={!!erroxText}
     {...field}
     {...props}
     />
    )
}
