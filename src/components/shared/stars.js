import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
const Stars = ({ rating, id }) => {
  return (
    <>
      {Array(rating)
        .fill()
        .map((_) => (
          <span key={Math.random()}>
            <FontAwesomeIcon
              style={{
                color: 'rgb(240, 133, 12)',
              }}
              className="faicons"
              icon="star"
            />
          </span>
        ))}
    </>
  );
};
export default Stars;
