import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useHistory } from 'react-router-dom';
import Headers from '../headerComponent/headers';
const Notification = () => {
  const history = useHistory();

  return (
    <>
      <Headers />
      <div className="container">
        <div onClick={() => history.goBack()}>
          <button
            className="btn btn-light"
            style={{ border: '.5px solid #e5e2e2', marginTop: '5px' }}
          >
            <FontAwesomeIcon className="faicons" icon="arrow-left" />
          </button>
        </div>
        <h3>Notification</h3>
        <hr />
        <ul className="list-group mb-4 checkout-invisible text-center">
          <li className="list-group-item list-group-item-danger d-flex justify-content-between lh-condensed">
            <div className="col">
              <h6>No new Notifications</h6>{' '}
            </div>
          </li>
        </ul>
      </div>
    </>
  );
};

export default Notification;
