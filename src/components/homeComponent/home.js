import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Header from '../headerComponent/headers';
import BurgerContainer from '../../container/burgerContainer';
import PizzaContainer from '../../container/PizzaContainer';
import CakesContainer from '../../container/cakesContainer';
import CookiesContainer from '../../container/Cookiescontainer';
import NavBarbottom from '../navBottomComponent/navBar-bottom';
import NavLinks from '../menuBarcomponent/navLinks';

function Home(props) {
  const { match } = props;
  return (
    <div>
      <Header />
      <NavLinks />
      <div className="container">
        <Switch>
          <Route path={`${match.path}/burger`} component={BurgerContainer} />
          <Route path={`${match.path}/pizza`} component={PizzaContainer} />
          <Route path={`${match.path}/cakes`} component={CakesContainer} />
          <Route path={`${match.path}/cookies`} component={CookiesContainer} />
        </Switch>
    </div>
      <NavBarbottom />
    </div>
  );
}

export default Home;
