import React from 'react';
import { Link } from 'react-router-dom';
import '../../styling/subTotal.css';
import { getCartTotal } from '../../reducers/product';
export const SubTotal = ({ cart }) => {
  return (
    <div className="subTototal ">
      <p>
        SubTotal ({cart.length}
        {cart.length === 1 ? 'item' : 'items'}):{` `}
        <strong style={{ fontSize: '12px' }}>
          {`RWF ${getCartTotal(cart)}`}
        </strong>
      </p>
      <Link
        to="/ProductContainer"
        className="btn"
        style={{
          borderRadius: '4px',
          margin: '0 auto',
          color: 'white',
          backgroundColor: 'var(--danger)',
          fontSize: '14px',
          padding: '4px 10px',
        }}
      >
        Proceed to checkout
      </Link>
    </div>
  );
};

export default SubTotal;
