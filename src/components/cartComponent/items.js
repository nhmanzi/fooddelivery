import React from 'react';
import { useDispatch } from 'react-redux';
import { decrementProduct, incrementProduct, removeFromCart } from '../../actions/product';
import '../../styling/cart.css';
const Items = ({ cart }) => {
  const dispatch = useDispatch();
  const handleRemove = (productId) => {
    dispatch(removeFromCart(productId));
  };
  const handleIncrement = (productId) => {
    dispatch(incrementProduct(productId));
  };
  const handleDecrement = (productId) => {
    dispatch(decrementProduct(productId));
  };
  return cart.length < 1 ? (
    <ul className="list-group mb-4 checkout-invisible text-center">
      <li className="list-group-item list-group-item-danger d-flex justify-content-between lh-condensed">
        <div className="col">
          <h6>Cart empty! add some Products</h6>{' '}
        </div>
      </li>
    </ul>
  ) : (
    cart.map((e) => (
      <div className="cart">
        <div className="cart__photo">
          <img src={e.photo} className="Product__photo"  alt={e.title}/>
        </div>
        <div className="product__details">
          <h5 className="mt-1">{e.title}</h5>
          <h6>
            <span className="currency">{e.price}RWF</span>
          </h6>
          <span className="product__functionality">
            <button
              style={{ border: '0.5px solid #e5e2e2' }}
              className="btn btn-light mr-0"
              id="left-btn"
              onClick={() => handleDecrement(e.product)}
              disabled={e.qty<=1}
            >
              <b> -</b>
            </button>
            <div className="product__quantity ">{e.qty}</div>
            <button
              style={{ border: '0.5px solid #e5e2e2' }}
              className="btn btn-light  ml-0 "
              id="right-btn"
              onClick={() => handleIncrement(e.product)}
            >
              <b> +</b>
            </button>
          </span>

          <button id="product__remove" onClick={() => handleRemove(e.product)}>
            Remove
          </button>
        </div>
      </div>
    ))
  );
};
// Items.propTypes = {
//   cart: PropTypes.arrayOf(PropTypes.object),
//   onDecrement: PropTypes.func,
//   onIncrement: PropTypes.func,
//   onRemove: PropTypes.func,
// };
export default Items;
