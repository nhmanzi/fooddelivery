import React from 'react';
import '../../styling/menuBar.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';

const NavLinks = () => {
  const ACTIVE = {
    backgroundColor: '#c44444 ',
    color: 'white',
  };

  return (
    <div className="container mt-2 mb-4">
      <div className="search">
        <input className="inputSearch" type="text" placeholder="Search" />
        <button className="btn btn-danger find">Find</button>
      </div>
      <ul className="nav-linky ">
        <li>
          <NavLink
            to="/product/burger"
            className="ACTIVE"
            activeStyle={ACTIVE}
            style={{ color: 'white' }}
          >
            <FontAwesomeIcon
              style={{
                color: 'white',
                backgroundColor: 'transparent',
              }}
              className="faicons"
              icon="hamburger"
            />
            {` `}
            Burger
          </NavLink>
        </li>

        <li>
          <NavLink
            to="/product/pizza"
            style={{ color: 'white' }}
            className="ACTIVE"
            activeStyle={ACTIVE}
          >
            <FontAwesomeIcon
              style={{
                color: 'white',
                backgroundColor: 'transparent',
              }}
              className="faicons"
              icon="pizza-slice"
            />
            {` `}
            Pizza
          </NavLink>
        </li>

        <li>
          <NavLink
            to="/product/cakes"
            className="ACTIVE"
            activeStyle={ACTIVE}
            style={{ color: 'white' }}
          >
            <FontAwesomeIcon
              style={{
                color: 'white',
                backgroundColor: 'transparent',
              }}
              className="faicons"
              icon="birthday-cake"
            />
            {` `}
            Cakes
          </NavLink>
        </li>

        <li>
          <NavLink
            to="/product/cookies"
            className="ACTIVE"
            activeStyle={ACTIVE}
            style={{ color: 'white' }}
          >
            <FontAwesomeIcon
              style={{
                color: 'white',
                backgroundColor: 'transparent',
              }}
              className="faicons"
              icon="cookie-bite"
            />
            {` `}
            cookies
          </NavLink>
        </li>
      </ul>
    </div>
  );
};

export default NavLinks;
