import React from 'react';
// import PropTypes from 'prop-types';
import ItemCookies from './itemCookies';
const Cookies = ({ cookies }) => {
  return <ItemCookies cookies={cookies} />;
};
// Cookies.propTypes = {
//   cookies: PropTypes.arrayOf(PropTypes.object),
//   onAdd: PropTypes.func,
// };
export default Cookies;
