import React, { useState } from 'react';
import {useHistory} from 'react-router-dom'
import '../../styling/header.css';
import { signOut } from '../../actions/user';
import { useDispatch } from 'react-redux';
const Headers = (props) => {
  const history=useHistory();
  const [showNav, setShowNav] = useState('dropdwon');
  const [lines, setLines] = useState('burger');
  function toggleNav() {
    let css = showNav === 'dropdwon' ? 'dropShow' : 'dropdwon';
    let css2 = lines === 'burger' ? 'toggle' : 'burger';
    setShowNav(css);
    setLines(css2);
  }
  const dispatch = useDispatch()
  const signOutUser=(e)=>{
    e.preventDefault();
   dispatch(signOut());
    history.push('/')
 }
  return (
    <React.Fragment>
  
     <nav className=" navbar1 stickyy">
          <div>
            <div style={{position:'absolute',top:"10px",left:0}} onClick={toggleNav} className={lines}>
              <div className="line1"></div>
              <div className="line2"></div>
              <div className="line3"></div>
            </div>
            <div className={showNav}>
              <h1 className="logo">FoodCo.</h1>
              <div>
                <ul className="sidebar__items">
                  <li>Home</li>
                  <li>About </li>
                  <li>Contact</li>
                  <li>Settings</li>
                </ul>
              </div>

              <button className=" sign__button" onClick={(e)=>signOutUser(e)}>Sign out</button>
            </div>
          </div>
          <div className="logo">
            <a className="brand-name m-0 p-0" href="/product/burger">
              FoodCo.
            </a>
          </div>
        </nav>

    </React.Fragment>
  );
};

export default Headers;
