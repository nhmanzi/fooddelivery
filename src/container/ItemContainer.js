import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
// import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import SubTotal from '../components/cartComponent/SubTotal';
import Items from '../components/cartComponent/items';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {addToCart} from '../actions/product';

export const ItemContainer = (props) => {
  const history = useHistory();
  const productID = props.match.params.id;
  const productItem = props.match.params.item;
  const qty = props.location.search
    ? Number(props.location.search.split('=')[1])
    : 1;
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;
  useEffect(() => {
    if (productID) dispatch(addToCart(productItem, productID, qty));
   
  }, [productItem, productID, qty,dispatch]);
  return (
    <div className="container">
      {cartItems.length >= 1 ? <SubTotal cart={cartItems} /> : null}
      <div onClick={() => history.goBack()}>
        <button
          className="btn btn-light"
          style={{ border: '.5px solid #e5e2e2', marginTop: '5px' }}
        >
          <FontAwesomeIcon className="faicons" icon="arrow-left" />
        </button>
      </div>

      <h1>cart list</h1>
      <hr />
      <Items cart={cartItems} />
    </div>
  );
};
// ItemContainer.propTypes = {
//   cart: PropTypes.arrayOf(PropTypes.object),
//   onIncrement: PropTypes.func.isRequired,
//   onDecrement: PropTypes.func.isRequired,
//   onRemove: PropTypes.func.isRequired,
// };
// const mapStateToProps = (store) => {
//   return {
//     cart: store.product.temp,
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     onIncrement: (title) => dispatch(incrementProduct(title)),
//     onDecrement: (title) => dispatch(decrementProduct(title)),
//     onRemove: (product) => dispatch(removeFromCart(product)),
//   };
// };

export default ItemContainer;
