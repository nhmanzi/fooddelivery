import React, { useEffect } from 'react';
import {  useDispatch, useSelector } from 'react-redux';
// import PropTypes from 'prop-types';
import Pizza from '../components/PizzaComponent/Pizza';
import { getPizzas } from '../actions/product';

export const PizzaContainer = () => {
  const pizzaList = useSelector((state) => state.pizzaList);
  const { pizza, loading } = pizzaList;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getPizzas());
    
  }, [dispatch]);

  return loading ? (
    <div >loading....</div>
  ) : (
    <Pizza pizza={pizza} />
  );
};
// PizzaContainer.propTypes = {
//   pizza: PropTypes.arrayOf(PropTypes.object),
//   handleAdd: PropTypes.func,
// };
// const mapStateToProps = (store) => ({
//   pizza: store.product.pizza,
// });

// const mapDispatchToProps = (dispatch) => {
//   return {
//     handleAdd: (product) => dispatch(addToCart(product)),
//   };
// };

export default PizzaContainer;
