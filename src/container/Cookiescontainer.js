import React, { useEffect } from 'react';
import {  useDispatch, useSelector } from 'react-redux';
// import PropTypes from 'prop-types';
import { getCookies } from '../actions/product';
import Cookies from '../components/cookieComponent/Cookies';

export const CookiesContainer = () => {
  const cookieList = useSelector((state) => state.cookieList);
  const { cookies, loading } = cookieList;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCookies());

  }, [dispatch]);

  return loading ? (
    <div >loading....</div>
  ) : (
    <Cookies cookies={cookies} />
  );
};
// CookiesContainer.propTypes = {
//   cookies: PropTypes.arrayOf(PropTypes.object),
//   handleAdd: PropTypes.func,
// };
// const mapStateToProps = (store) => ({
//   cookies: store.product.cookies,
// });

// const mapDispatchToProps = (dispatch) => {
//   return {
//     handleAdd: (product) => dispatch(addToCart(product)),
//   };
// };

export default CookiesContainer;
