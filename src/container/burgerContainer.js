import React, { useEffect } from 'react';
// import PropTypes from 'prop-types';
import {  getBurgers } from '../actions/product';
import {  useDispatch, useSelector } from 'react-redux';
import Burgers from '../components/burgerComponent/Burgers';

export const BurgerContainer = () => {
  const burgerList = useSelector((state) => state.burgerList);
  const { burgers, loading} = burgerList;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getBurgers());
    
  }, [dispatch]);
  return loading ? (
    <div >loading....</div>
  ) : (
    <Burgers burgers={burgers} />
    
  );
};
// BurgerContainer.propTypes = {
//   burger: PropTypes.arrayOf(PropTypes.object),
//   handleAdd: PropTypes.func,
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     handleAdd: (product) => dispatch(addToCart(product)),
//   };
// };

export default BurgerContainer;
