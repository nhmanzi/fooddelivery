import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
// import PropTypes from 'prop-types';
import Cakes from '../components/cakeComponent/Cakes';
import { getCakes } from '../actions/product';
const CakesContainer = () => {
  const cakeList = useSelector((state) => state.cakeList);
  const { cakes, loading} = cakeList;
  const dispatch = useDispatch();
  useEffect(() => {
   
    dispatch(getCakes());
  }, [dispatch]);

  return loading ? (
    <div >loading....</div>
  ) : (
    <Cakes cakes={cakes} />
  );
};
// cakesContainer.propTypes = {
//   cakes: PropTypes.arrayOf(PropTypes.object),
//   handleAdd: PropTypes.func,
// };
// const mapStateToProps = (store) => ({
//   cakes: store.product.cakes,
// });

// const mapDispatchToProps = (dispatch) => {
//   return {
//     handleAdd: (product) => dispatch(addToCart(product)),
//   };
// };
export default CakesContainer;
