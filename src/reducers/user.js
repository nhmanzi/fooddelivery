import * as types from '../constants'
import cookie from 'js-cookie'
import jwt_decode from "jwt-decode";
export const user = (state = {}, action) => {
  switch (action.type) {
    case types.USER_SIGNIN_REQUEST:
     return {loading:true}
      case types.USER_SIGNIN_SUCCESS:
        console.log('USER PAYLOAD',action.payload)
        const token=action.payload.data.token
        const userInfos=jwt_decode(token)
        console.log('USER INFOOOO',userInfos)
         cookie.set('userInfos',JSON.stringify(userInfos))
        return {loading:false, userInfos}
      case types.USER_SIGNIN_FAIL:
        console.log('error', action.payload)
        return{loading:false, error:action.payload}
        case types.USER_SIGNOUT:
console.log('twahagexeee wanaa')
          // cookie.set('userInfos',null)
        return {userInfos:null,error:null,loading:false}
        
    default:
      return state
      
  }
};

export const userRegister = (state = {}, action) => {
  switch (action.type) {
    case types.USER_REGISTER_REQUEST:
     return {loading:true}
      case types.USER_REGISTER_SUCCESS:
        console.log('USER PAYLOAD',action.payload) 
        return {loading:false, message:action.payload}
      case types.USER_REGISTER_FAIL:
        console.log('error', action.payload)
        return{loading:false, error:action.payload}
        
    default:
      return state
      
  }
};