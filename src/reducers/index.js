import { combineReducers } from 'redux';
import {
  burgerReducer,
  pizzaReducer,
  cakesReducer,
  cookiesReducer,
  detailReducer,
  cartReducer,
} from './product';
import {user,userRegister} from './user';

export default combineReducers({
  burgerList: burgerReducer,
  pizzaList: pizzaReducer,
  cakeList: cakesReducer,
  cookieList: cookiesReducer,
  productDetail: detailReducer,
  cart: cartReducer,
  userSignIn:user,
  userRegister
});
