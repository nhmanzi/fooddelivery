import * as types from '../constants';

export const getCartTotal = (temp) =>
  temp.reduce((amount, item) => item.price * item.qty + amount, 0);

export const burgerReducer = (state = { burgers: [] }, action) => {
  switch (action.type) {
    case types.BURGER_LIST_REQUEST:
      return { loading: true };
    case types.BURGER_LIST_SUCCESS:
      return { loading: false, burgers: action.payload };
    case types.BURGER_LIST_FAIL:
      return { loading: false, error: action.payload };
    case types.BURGER_ADDED:
      const id = action.payload;
      const productIndex = state.burgers.findIndex((c) => c.id === id);
      const newState = state.burgers.map((item, index) => {
        if (index !== productIndex) {
          return item;
        }
        return { ...item, isAdded: true};
      });
      console.log('ngahoooe',newState)
      return { loading: false, burgers: newState };
    default:
      return state;
  }
};
export const cookiesReducer = (state = { cookies: [] }, action) => {
  switch (action.type) {
    case types.COOKIES_LIST_REQUEST:
      return { loading: true };
    case types.COOKIES_LIST_SUCCESS:
      return { loading: false, cookies: action.payload };
    case types.COOKIES_LIST_FAIL:
      return { loading: false, error: action.payload };
    case types.COOKIE_ADDED:
      const id = action.payload;
      const productIndex = state.cookies.findIndex((c) => c.id === id);
      const newState = state.cookies.map((item, index) => {
        if (index !== productIndex) {
          return item;
        }
        return { ...item, isAdded: true };
      });
      console.log('NEWSTATE', newState);
      return { loading: false, cookies: newState };
    default:
      return state;
  }
};
export const cakesReducer = (state = { cakes: [] }, action) => {
  switch (action.type) {
    case types.CAKES_LIST_REQUEST:
      return { loading: true };
    case types.CAKES_LIST_SUCCESS:
      return { loading: false, cakes: action.payload };
    case types.CAKES_LIST_FAIL:
      return { loading: false, error: action.payload };
    case types.CAKE_ADDED:
      const id = action.payload;
      const productIndex = state.cakes.findIndex((c) => c.id === id);
      const newState = state.cakes.map((item, index) => {
        if (index !== productIndex) {
          return item;
        }
        return { ...item, isAdded: true };
      });
      console.log('NEWSTATE', newState);
      return { loading: false, cakes: newState };
    default:
      return state;
  }
};
export const pizzaReducer = (state = { pizza: [] }, action) => {
  switch (action.type) {
    case types.PIZZA_LIST_REQUEST:
      return { loading: true };
    case types.PIZZA_LIST_SUCCESS:
      return { loading: false, pizza: action.payload };
    case types.PIZZA_LIST_FAIL:
      return { loading: false, error: action.payload };
    case types.PIZZA_ADDED:
      const id = action.payload;
      const productIndex = state.pizza.findIndex((c) => c.id === id);
      const newState = state.pizza.map((item, index) => {
        if (index !== productIndex) {
          return item;
        }
        return { ...item, isAdded: true };
      });
      console.log('NEWSTATE', newState);
      return { loading: false, pizza: newState };
    default:
      return state;
  }
};

export const detailReducer = (state = { product: {} }, action) => {
  switch (action.type) {
    case types.PRODUCT_DETAIL_REQUEST:
      return { loading: true };
    case types.PRODUCT_DETAIL_SUCCESS:
      return { loading: false, product: action.payload };
    case types.PRODUCT_DETAIL_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const cartReducer = (state = { cartItems: [] }, action) => {
  switch (action.type) {
    case types.ADD_TO_CART:
      const item = action.payload;
      const product = state.cartItems.find((x) => x.product === item.product);
      if (product) {
        return {
          ...state,
          cartItems: state.cartItems.map((x) =>
            x.product === product.product ? product : x
          ),
        };
      }
      return { cartItems: [...state.cartItems, item] };
    case types.REMOVE_FROM_CART:
      return {
        cartItems: state.cartItems.filter((c) => c.product !== action.payload),
      };
      case types.INCREMENT_QUANTITY:
         const id = action.payload;
      const productIndex = state.cartItems.findIndex((c) => c.product === id);
      const newState = state.cartItems.map((item, index) => {
        if (index !== productIndex) {
          return item;
        }
        return { ...item, qty:item.qty+1};
      });
      return { cartItems: [...newState] }
      
      case types.DECREMENT_QUANTITY:
        const id1 = action.payload;
        const productIndex1 = state.cartItems.findIndex((c) => c.product === id1);
        const newState1 = state.cartItems.map((item, index) => {
          if (index !== productIndex1) {
            return item;
          }
          return { ...item, qty:item.qty-1};
        });
        return { cartItems: [...newState1] }
    default:
      return state;
  }
};
