import axios  from 'axios'
export const userSignInRequest = async(values)  => {
    try {
        const { data } = await axios.post(
          'http://localhost:5000/api/auth/login',
          values
        )
       
      return data


    }catch(error){
        return error
     }
};


export const userRegisterInRequest = async(values)  => {
    try {
        const { data } = await axios.post(
          'http://localhost:5000/api/auth/signup',
          values
        )
       console.log('incomminggggg user',data)

        return {message:'User created!'}


}catch(error){

return error.response.data.message
}
};