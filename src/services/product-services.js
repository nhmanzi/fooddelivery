import axios from 'axios'
export const fetchBurgers =async (url)  => {
    try {
      const { data } = await axios.get(
        'http://localhost:5000/api/products/getBurgers'||url
      );
  
     return data
    } catch (error) {
      return error.message
  }
}
export const fetchSingleBurger =async (id)  => {
    try {
      const { data } = await axios.get(
        'http://localhost:5000/api/products/getBurgers/'+id
      );
  
     return data
    } catch (error) {
      return error.message
  }
}
export const fetchCookies = async(url)  => {
    try {
      const { data } = await axios.get(
        'http://localhost:5000/api/products/getCookies'|| url
      );
    
  return data
    } catch (error) {
   return error.message
    }
  };
  export const fetchSingleCookie = async(id)  => {
    try {
      const { data } = await axios.get(
        'http://localhost:5000/api/products/getCookies/'+id
      );
    
  return data
    } catch (error) {
   return error.message
    }
  };
  export const fetchPizza=async (url)  => {
    try {
     
      const { data } = await axios.get(
        'http://localhost:5000/api/products/getPizzas'|| url
      );
  
  return data
    } catch (error) {
 return error.message
    }
  };
  export const fetchSinglePizza=async (id)  => {
    try {
     
      const { data } = await axios.get(
        'http://localhost:5000/api/products/getPizzas/'+id
      );
  
  return data
    } catch (error) {
 return error.message
    }
  };
  export const fetchCakes = async(url)  => {
    try {
  
      const { data } = await axios.get(
        'http://localhost:5000/api/products/getCakes'|| url
      );
  return  data
    } catch (error) {
      return  error.message
    }
  };
  export const fetchSingleCake = async(id)  => {
    try {
  
      const { data } = await axios.get(
        'http://localhost:5000/api/products/getCakes/'+id
      );
  return  data
    } catch (error) {
      return  error.message
    }
  };


  